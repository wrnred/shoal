package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/", getIndex)
	router.Run("localhost:8080")
}

// getAlbums envoye le message par defaut.
func getIndex(c *gin.Context) {
	c.Writer.WriteHeader(418)
}
